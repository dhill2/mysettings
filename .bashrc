#set vars
export PATH="/Applications/MAMP/bin/php/php5.4.10/bin:/Users/dhill2/composer-packages/vendor/bin:/Users/dhill2/bin:$PATH"
export HISTTIMEFORMAT='%b %d %I:%M %p '
export HISTCONTROL=ignoreboth
export HISTIGNORE="h:history:pwd:exit:df:ls:ls -la:ll"



terri() {
    DIRPATH=$1
    shift

    for VAR in "${@}"; do
        echo "$DIRPATH/$VAR"
    done

}



tdir() {

    charpath=${PWD%/*/*}
    if [[ "$OSTYPE" == "darwin"* ]]; then
        charpath=$(echo $charpath | sed -E 's|/(.)[^/]*|/\1|g')
    else
        charpath=$(echo $charpath | sed -r 's|/(.)[^/]*|/\1|g')
    fi

    tdir=$(pwd |rev| awk -F / '{print $1,$2}' | rev | sed s_\ _/_)
    echo "$charpath/$tdir"
}

export PS1='\[\033[1;35m\] \t\e[m \e[0;32m$(tdir)\e[m $ '
#export PS1='\[\033[1;35m\] \t\e[m \e[0;32m\w\e[m $ '
export GREP_OPTIONS="--color=auto"

#basic
alias ll='ls -lahG'
alias up='cd ..'
alias h='history'

#connect to dcgc
alias dcgc='ssh digitalcreative.gannett.com@s137273.gridserver.com'
alias azure='ssh dev-dhill2@giadc-reports.cloudapp.net'

. /Users/dhill2/z-master/z.sh

#flash/jsfl
alias copyJsflToRepo="cp -Rf /Users/dhill2/Library/Application\ Support/Adobe/Flash\ CS6/en_US/Configuration/WindowSWF/TestJsfl/ /Users/dhill2/Desktop/repos/digital-toolbox/ToolboxScripts/"
alias copyRepoToJsfl="cp -Rf /Users/dhill2/Desktop/repos/digital-toolbox/ToolboxScripts/ /Users/dhill2/Library/Application\ Support/Adobe/Flash\ CS6/en_US/Configuration/WindowSWF/TestJsfl/"
alias copyRepoToJsflPro="cp -Rf /Users/dhill2/Desktop/repos/digital-toolbox/ToolboxScripts/ /Users/dhill2/Library/Application\ Support/Adobe/Flash\ CS6/en_US/Configuration/WindowSWF/ToolboxScripts/"
alias copyProToRepo="cp -Rf /Users/dhill2/Library/Application\ Support/Adobe/Flash\ CS6/en_US/Configuration/WindowSWF/ToolboxScripts/ /Users/dhill2/Desktop/repos/digital-toolbox/ToolboxScripts/"
alias copySwfToWindowSwf="cp -f /Users/dhill2/Desktop/repos/digital-toolbox/DigitalToolbox.swf /Users/dhill2/Library/Application\ Support/Adobe/Flash\ CS6/en_US/Configuration/WindowSWF/"
alias moveSwf="cp -i /Users/dhill2/Library/Application\ Support/Adobe/Flash\ CS5.5/en_US/Configuration/WindowSWF/toolboxPanel.swf /Users/dhill2/Desktop/repos/pointroll-lite/PRLiteToolbox/PRLiteToolbox.swf"
alias moveSwfVid="cp -i /Users/dhill2/Library/Application\ Support/Adobe/Flash\ CS5.5/en_US/Configuration/WindowSWF/VideoBuilderTEST.swf /Users/dhill2/Desktop/repos/pointroll-lite/VideoPanelBuilder/VideoBuilder.swf"
alias upToolbox="open ~/Desktop/repos/pointroll-lite/PRLiteToolbox/PRLiteToolbox.mxi"
alias inToolbox="open ~/Desktop/PRLiteToolbox.mxp"
alias createZip="sh ~/Desktop/repos/digital-toolbox/_DEV_/createZip.sh"

#Sublime
alias sub="open -a 'Sublime Text.app'"

#php
alias serv='php -S localhost:8000; open -a "Google Chrome.app" http://localhost:8000/'

#errors
alias xLog="tail 10 /Applications/MAMP/logs/php_error.log"
alias fullLog="cat /Applications/MAMP/logs/php_error.log"
alias watchLog="tail -f /Applications/MAMP/logs/php_error.log"

#shortcuts
alias goReports='cd /Volumes/TEMPLATES/GIADC_Reports/dbc/'
alias goRepo='cd /Users/dhill2/Desktop/done/junk/repos'
alias goVM='cd ~/Desktop/vm'
alias goDesktop='cd ~/Desktop/'
alias goPR='cd /Volumes/General/PRScripts/'
alias goWindowSWF="cd /Users/dhill2/Library/Application\ Support/Adobe/Flash\ CS6/en_US/Configuration/WindowSWF"

#MySQL
alias openSQL="/Applications/MAMP/Library/bin/mysql -uroot -proot"

#utils
alias showRC="subl ~/.bashrc"
alias bsource='source ~/.bashrc'
alias showCReports='open -a "Google Chrome.app" https://dvatol.gannettproduction.com/GIADC_Reports/dbc/html/views/index.php'
alias sLocal='open -a "Google Chrome.app" http://localhost:8080/'
alias fla='open -a "Adobe Flash CS6.app"'
alias bit='open -a "Google Chrome.app" https://bitbucket.org/giadc_salamander'
alias subl='open -a "Sublime Text.app"'
alias xBreak='banner -w 35 BREAK'
alias jira="open https://gannett.jira.com/secure/BrowseProjects.jspa#10700"
alias jiraCC="open https://gannett.jira.com/secure/RapidBoard.jspa?rapidView=239&view=detail&selectedIssue=GIADCCC-30"

#phpunit
alias pu="phpunit --colors"

# git aliases
alias gs='git status '
alias ga='git add '
alias gb='git branch '
alias gc='git commit'
alias gd='git diff'
alias go='git checkout '
alias gf='git fetch '
alias gl='git log '
#pretty log
alias glc='git log --graph --all --pretty=format:"%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit'
alias gk='gitk --all&'
alias gx='gitx --all'

#laravel aliases
alias art='php artisan'
alias dbms='art migrate:refresh --seed'
alias as='open http://localhost:8000 && php artisan serve'
# using jeffrey way's generators
alias g:c='art generate:controller'
alias g:f='art generate:form'
alias g:mig='art generate:migration'
alias g:m='art generate:model'
alias g:p='art generate:pivot'
alias g:r='art generate:resource'
alias g:scaf='art generate:scaffold'
alias g:s='art generate:seed'
alias g:t='art generate:test'
alias g:v='art generate:view'

# composer aliases
alias cda='composer dump-autoload -o'
alias cu='composer update'

#vagrant aliases
alias vs='vagrant status'
alias vu='vagrant up'
alias vh='vagrant halt'
alias vd='vagrant destroy -f'
alias vf="VBoxManage list runningvms -l | grep 'Host path\|guest port = 80' | sed \"s/.*host\ port\ =\ \([0-9]*\),.*/Port: \1/\" | sed \"s/.*path:\ '\([a-zA-Z\/]*\).*/Path: \1/\""
alias lagrant="git clone https://github.com/davidxhill/Lagrant -b davidxhill/mysql-permissions && mv Lagrant/* ./ && rm -r -f Lagrant && vagrant up"

alias testLagrant="rm -rf ~/Desktop/Lagrant && mkdir ~/Desktop/Lagrant && cd ~/Desktop/Lagrant && cp -R -f ~/Desktop/vm/Lagrant/ ~/Desktop/Lagrant/ && sh ~/Desktop/Lagrant/setup.sh"

# phpunit with composer
alias t='~/composer-packages/vendor/bin/phpunit'

# runs a rot13 on a string and copies it to your clipboard. cheap encryption
# mix cat // png
function mix(){
   echo -n $1 | tr 'A-Za-z' 'N-ZA-Mn-za-m' | pbcopy
}

# make a directory and cd into it
function mkcd() {
  mkdir -p "$1" && cd "$1";
}

# redefine commands to include popular options
# alias mv='mv -i'
# alias cp='cp -i'
# alias rm='rm -i'
alias df='df -h'
alias du='du -h'
alias mkdir='mkdir -p'

# fixing common typos
alias pdw='pwd'

#random functions
#########################################

updateSettings() {
	export now=$(date +"%s")
	export current=$(pwd)

	cp -f ~/.vimrc.local ~/Desktop/mySettings/.vimrc.local
	cp -f ~/.bashrc ~/Desktop/mySettings/.bashrc 

	cd ~/Desktop/mySettings

	git add .
	git commit -m "Settings Update - $now"
	git push

	cd $current
}

getSettings() {
	export current=$(pwd)

	cd ~/Desktop/mySettings
	git pull

	cp -f ~/Desktop/mySettings/.vimrc.local ~/.vimrc.local 
	cp -f ~/Desktop/mySettings/.bashrc ~/.bashrc 

	cd $current
}


#move into a directory and list contents
cc(){
	cd $1
	ll
}

# Opens text file(password required)
getPW(){
	openssl des3 -d -salt -in ~/Desktop/textFile.txt -out ~/Desktop/TempFile.txt
	open ~/Desktop/TempFile.txt
}

# Removes tempFile and secures updated text file
hidePW(){
	openssl des3 -salt -in ~/Desktop/TempFile.txt -out ~/Desktop/textFile.txt
	rm ~/Desktop/TempFile.txt
}

# Create and open with SublimeText
create(){
	touch $1
	vim $1
}

# Creates php file with default class text
newClass(){
	export class=$(echo $1 | grep -Eo '[A-Za-z]+$')
	export namespace=$(echo $1 | tr / \\)

	if [[ $2 == "" ]]; then
		export extends=$(echo "")
	else
		export extends=$(echo "extends $2")
	fi
	
	echo -e "<?php namespace $namespace; \n\n\tclass $class $extends{\n\n\t\tpublic function __construct(){\n\t\t\t#codeHere...\n\t\t}\n\n}" > $1.php
}
